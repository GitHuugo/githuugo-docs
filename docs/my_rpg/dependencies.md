---
sidebar_position: 1
---

# Dependencies

## Code
```c title="/include/dependencies.h"
#ifndef DP_H_
    #define DP_H_
    ////////////////////////////////// INCLUDES //////////////////////////////////
    #include "./my.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <SFML/Window/Export.h>
    #include <SFML/Window/Event.h>
    #include <SFML/Window/VideoMode.h>
    #include <SFML/Window/WindowHandle.h>
    #include <SFML/Window/Types.h>
    #include <SFML/System/Vector2.h>
    #include <SFML/Graphics/RenderWindow.h>
    #include <SFML/Graphics.h>
    #include <SFML/System/Vector2.h>
    #include <SFML/Audio.h>
    #include <SFML/Audio/Sound.h>
    #include <SFML/System.h>
    ////////////////////////////////// STRUCTURES //////////////////////////////////
    typedef struct window_s {
        sfEvent evt;
        sfVideoMode m;
        sfRenderWindow *w;
    } window_t;
    typedef struct menu_s {
        sfSprite *ms[10];
        sfSprite *os[10];
    } menu_t;
    typedef struct game_s {
        sfSprite *gs[10];
    } game_t;
    typedef struct game_audio_s {
        sfMusic *m[2];
        int vl[2];
        int st[2];
    } audio_t;
    typedef struct pack_s {
        window_t w;
        menu_t m;
        game_t g;
        audio_t a;
        int gwm;
    } pack_t;
    ////////////////////////////////// FONCTIONS //////////////////////////////////
    int sp_ini(sfSprite *sprite, sfVector2f pos, sfVector2f scl, char *img);
    int error_handling(int argc, char **argv);
    int window_start(pack_t *p);
    int display_menu(pack_t *p);
    int menu_events(pack_t *p);

#endif /* DP_H_ */
```

## Includes
#### La partie include ne nous intéresse pas vraiment ici, je vais la modifier dans le futur car elle contient pas mal de choses inutiles. N'hésitez pas à ajouter les includes dont vous avez besoin ici.

## Structures

### Pack_s
#### La structure `pack_s` qui est la structure qui contient tous les autres, elle est appelée une fois au début de `rpg_main.c` et a pour seule fonction d'être déplacée dans le fichier pour qu’une seule structure ne soit utilisée.
```c
typedef struct pack_s {
    window_t w;
    menu_t m;
    game_t g;
    audio_t a;
    int gwm;
} pack_t;
```
:::danger ATTENTION
La fonction `pack_s` ne doit être appelée que dans `rpg_main.c`
:::
| Appel dans le code | Signification | Type |
| :----------------- | :------------ | :--- |
| `p->w` | `pack->window` | `struct window_t w;` |
| `p->m` | `pack->menu` | `struct menu_t m;` |
| `p->g` | `pack->game` | `struct game_t g;` |
| `p->a` | `pack->audio` | `struct audio_t a;` |
| `p->gwm` | `pack->game-window-mode` | `int gwm;` |

### Window_s
#### La structure `window_s` contient toutes les information nécessaire à la window.
```c
typedef struct window_s {
    sfRenderWindow *w;
    sfEvent evt;
    sfVideoMode m;
} window_t;
```
| Appel dans le code | Signification | Type |
| :----------------- | :------------ | :--- |
| `p->w.w` | `pack->window.window` | `sfRenderWindow *w;` |
| `p->w.evt` | `pack->window.event` | `sfEvent evt;` |
| `p->w.m` | `pack->window.mode` | `sfVideoMode m;` |

### Menu_s
#### La structure `menu_s` contient tous les sprite nécessaire au l’affichage du menu.
```c
typedef struct menu_s {
    sfSprite *ms[10];
    sfSprite *os[10];
} menu_t;
```
| Appel dans le code | Signification | Type |
| :----------------- | :------------ | :--- |
| `p->m.ms[x]` | `pack->menu.menu-sprite[x]` | `sfSprite *ms[10];` |
| `p->m.os[x]` | `pack->menu.option-sprite[x]` | `sfSprite *os[10];` |

### Game_s
#### La structure `game_s` contient tous les sprite nécessaire au l’affichage du jeu.
```c
typedef struct game_s {
    sfSprite *gs[10];
} game_t;
```
| Appel dans le code | Signification | Type |
| :----------------- | :------------ | :--- |
| `p->g.gs[x]` | `pack->game.game-sprite[x]` | `sfSprite *gs[10];` |

### Audio_s
#### La structure `audio_s` contient toutes les musique et leur status.
```c
typedef struct audio_s {
    sfMusic *m[2];
    int vl[2];
    int st[2];
} audio_t;
```
| Appel dans le code | Signification | Type |
| :----------------- | :------------ | :--- |
| `p->a.m[x]` | `pack->audio.music[x]` | `sfMusic *m[2];` |
| `p->a.vl[x]` | `pack->audio.volume[x]` | `int vl[2];` |
| `p->a.st[x]` | `pack->audio.state[x]` | `int st[2];` |

## Fonctions
#### La partie fonctions, contient toutes les fonctions de notre programme a besoin pour fonctionner. N'hésitez pas à ajouter les vôtres à la suite.


## Exemples

### EX: includes
#### Exemple: l’on souhaite ajouter les #include lié à la fonction `open()`
```c title="/include/dependencies.h"
    ////////////////////////////////// INCLUDES //////////////////////////////////
    #include "./my.h"
    #include <stdlib.h>
    #include <stdio.h>
    #include <SFML/Window/Export.h>
    #include <SFML/Window/Event.h>
    #include <SFML/Window/VideoMode.h>
    #include <SFML/Window/WindowHandle.h>
    #include <SFML/Window/Types.h>
    #include <SFML/System/Vector2.h>
    #include <SFML/Graphics/RenderWindow.h>
    #include <SFML/Graphics.h>
    #include <SFML/System/Vector2.h>
    #include <SFML/Audio.h>
    #include <SFML/Audio/Sound.h>
    #include <SFML/System.h>       
    #include <sys/stat.h>   // + open() include(1)  
    #include <fcntl.h>     //  + open() include(2)
```

### EX: structures
#### Exemple: l’on souhaite supprimer la window a l’aide de la fonction [`sfRenderWindow_destroy()`](https://26.customprotocol.com/csfml/RenderWindow_8h.htm#a6d504a27e7dab732c8a26d19e2cb61a7)
```c title="/src/rpg_window.c"
int window_start(pack_t *p)
{
    sfRenderWindow_destroy(p->w.w);
    return (0);
}
```
#### Exemple: l’on souhaite fermer la fenêtre lorsque l'événement est de type [`sfEvtClosed`](https://26.customprotocol.com/csfml/Event_8h.htm#a18e8028e83dbf54a65b18b465634e2f9a1e0899b43e06b867157f1576cce3700d), a l’aide de la fonction [`sfRenderWindow_close()`](https://26.customprotocol.com/csfml/RenderWindow_8h.htm#a3b203189a7160e1e64c299e82f03de02)
```c
int event_window(pack_t *p)
{
    if (p->w.evt.type == sfEvtClosed)
        sfRenderWindow_close(p->w.w);
    return (0);
}
```

### EX: Fonctions
#### Exemple: l’on souhaite ajouter la fonction `veci()` que l’on vient de créer (`sfVector2i veci(int x, int y)`)
```c
    int sp_ini(sfSprite *sprite, sfVector2f pos, sfVector2f scl, char *img);
    int error_handling(int argc, char **argv);
    int window_start(pack_t *p);
    int display_menu(pack_t *p);
    int menu_events(pack_t *p);
    sfVector2i veci(int x, int y);      // + veci() fonction
```
:::info
`veci()` est la contraction de `sfVector2i()`
:::