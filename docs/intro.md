---
sidebar_position: 1
---

# Introduction

:::info
Vous m'excuserez si c'est pas parfait, c'est ma première documentation.
:::

## Fichier de base:
![Docusaurus logo](/img/base.png)

## Pour faire simple:
#### - Le fichier `audio` contient tous les fichiers, comme son nom l'indique, audio tels de les musiques et autres bruitages.. Le dossier `img` contient quant à lui toutes les images dont nous avons besoin: les textures, sprite sheet et autres.
#### - `/include/dependencies.h` contient beaucoup de choses très utile et importante j’ai consacrée une page entière à ce fichier disponible: [ici](./my_rpg/dependencies) (n’hésitez pas à aller le lire, c’est une page assez importante). 
#### - Le fichier `src` contient toutes nos sources tels que le main, tools.. 
- les fichier s’appel `rpg_main.c` pour l'indexation car il sont des fichier ‘globaux’ par exemple `rpg_display.c` sert à définir la scène à afficher, entre autres, elle est donc global. `menu_events.c` sert à gérer les events tels que les cliques de souris, interaction avec des touches et autres mais uniquement dans le menu. A l’avenir viendra `game_events.c` qui ne gère les events que dans le jeu en lui même. 
- Certain fichier contiennent des fonctions assez importantes, j’ai donc également créé une page disponible ici pour vous aider à mieux comprendre certaines choses.